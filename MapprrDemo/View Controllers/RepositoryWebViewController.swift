//
//  RepositoryWebViewController.swift
//  MapprrDemo
//
//  Created by SUBAHAN on 23/11/18.
//  Copyright © 2018 SUBAHAN. All rights reserved.
//

import UIKit
import WebKit

class RepositoryWebViewController: UIViewController,WKNavigationDelegate {
    
    var repositoryRepoUrl:String!
    var sv : UIView?

    @IBOutlet weak var webView: WKWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        startLoader()
        let url = URL(string: repositoryRepoUrl ?? "")
        webView.load(URLRequest(url: url!))
        webView.navigationDelegate = self
        
    }

    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        stopLoader()
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        stopLoader()
    }
    func startLoader(){
        sv = UIViewController.displaySpinner(onView: self.view)
    }
    func stopLoader(){
        if let loaderView = sv{
            UIViewController.removeSpinner(spinner: loaderView)
        }
    }
    


}
