//
//  ReposListViewController.swift
//  MapprrDemo
//
//  Created by SUBAHAN on 25/11/18.
//  Copyright © 2018 SUBAHAN. All rights reserved.
//

import UIKit
import AlamofireImage
import Alamofire

class ReposListViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var contributorImage: UIImageView!
    @IBOutlet weak var reposListTableView: UITableView!
    
    var selectedContributor : ContributionsList!

    
    var reposList : [GitHubResponse.Items] = []{
        didSet{
            reposListTableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reposList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let repositoryCell = tableView.dequeueReusableCell(withIdentifier: "ReposCell", for: indexPath) as? ReposListTableViewCell
        let currentRepo = reposList[indexPath.row]
        
        repositoryCell?.repoName.text = currentRepo.name! 
       
        return repositoryCell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let currentRepo = reposList[indexPath.row]
        performSegue(withIdentifier: "GotoRepoDetails", sender: currentRepo)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "GotoRepoDetails"{
            let destinationVC = segue.destination as? RepositoryDetailsViewController
            destinationVC?.selectedRepository = sender as? GitHubResponse.Items
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        reposList = []
        self.navigationItem.title = (selectedContributor.login ?? "").capitalized

        
        getRepos(contributorName: selectedContributor.login!)

        contributorImage.af_setImage(withURL: URL.init(string: selectedContributor.avatar_url ?? "")!)
        
        reposListTableView.tableFooterView = UIView.init(frame: .zero)
        reposListTableView.estimatedRowHeight = 190
        reposListTableView.rowHeight = UITableView.automaticDimension
        self.reposListTableView.reloadData()
        
    }
    
    func getRepos(contributorName : String){
        let todoEndpoint: String = "https://api.github.com/users/\(contributorName)/repos"
        let sv = UIViewController.displaySpinner(onView: self.view)
        Alamofire.request(todoEndpoint)
            .responseJSON {[weak self] response in
                // check for errors
                guard response.result.error == nil else {
                    // got an error in getting the data, need to handle it
                    print("error calling GET on /todos/1")
                    print(response.result.error!)
                    UIViewController.removeSpinner(spinner: sv)
                    return
                }
                
                // make sure we got some JSON since that's what we expect
                if let json = response.result.value as? [[String: Any]]{
                    self?.reposList = GitHubResponse.Items.modelsFromDictionaryArray(array: json as NSArray)
                    UIViewController.removeSpinner(spinner: sv)
                    
                } else {
                    print("didn't get  object as JSON from API")
                    if let error = response.result.error {
                        print("Error: \(error)")
                    }
                    UIViewController.removeSpinner(spinner: sv)
                    return
                }
                
        }
    }


}

class ReposListTableViewCell : UITableViewCell{
    
    @IBOutlet weak var repoName: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
}
