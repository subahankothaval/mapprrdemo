//
//  RepositoryDetailsViewController.swift
//  MapprrDemo
//
//  Created by SUBAHAN on 23/11/18.
//  Copyright © 2018 SUBAHAN. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class RepositoryDetailsViewController: UIViewController,UICollectionViewDataSource, UICollectionViewDelegate {

    @IBOutlet weak var contributorsCollectionView: UICollectionView!
    
    var selectedRepository : GitHubResponse.Items!{
        didSet{
            loadData()
        }
    }
    @IBOutlet weak var repoImageView: UIImageView!
    @IBOutlet weak var repoNameLabel: UILabel!
    @IBOutlet weak var repoLinkButton: UIButton!
    @IBOutlet weak var repoDescriptionLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    func loadData(){
        self.navigationItem.title = (selectedRepository.name ?? "").capitalized

        
        repoImageView.af_setImage(withURL: URL.init(string: selectedRepository.owner?.avatar_url ?? "")!)
        repoNameLabel.text = selectedRepository.name ?? ""
        repoDescriptionLabel.text = selectedRepository.description ?? ""
        getContributorsList(contributor: selectedRepository.full_name!)
    }
    
    var contributorsList : [ContributionsList] = []{
        didSet{
            contributorsCollectionView.reloadData()
        }
    }
    
    func getContributorsList(contributor : String){
        let todoEndpoint: String = "https://api.github.com/repos/\(contributor)/contributors"
        let sv = UIViewController.displaySpinner(onView: self.view)
        Alamofire.request(todoEndpoint)
            .responseJSON {[weak self] response in
                // check for errors
                guard response.result.error == nil else {
                    // got an error in getting the data, need to handle it
                    print("error calling GET on /todos/1")
                    print(response.result.error!)
                    UIViewController.removeSpinner(spinner: sv)
                    return
                }
                
                // make sure we got some JSON since that's what we expect
                if let json = response.result.value as? [[String: Any]]{
                    self?.contributorsList =   ContributionsList.modelsFromDictionaryArray(array: json as NSArray)
                    UIViewController.removeSpinner(spinner: sv)
                } else {
                    print("didn't get  object as JSON from API")
                    if let error = response.result.error {
                        print("Error: \(error)")
                    }
                     UIViewController.removeSpinner(spinner: sv)
                    return
                }
                
        }
    }
    



    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return contributorsList.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "contributorcell", for: indexPath as IndexPath) as! ContributorsCell
        let currentRepo = contributorsList[indexPath.row]
        cell.contibutorsImage.af_setImage(withURL: URL.init(string: currentRepo.avatar_url ?? "")!);
        cell.contributorName.text = currentRepo.login ?? ""
        return cell
    }
    
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        print("You selected cell #\(indexPath.item)!")
        
        let currentRepo = contributorsList[indexPath.row]
        let repoDetailsVC = UIStoryboard.init(name: "MapprrDemo", bundle: nil).instantiateViewController(withIdentifier: "ReposListViewControllerID") as? ReposListViewController
        repoDetailsVC?.selectedContributor = currentRepo
        self.navigationController?.pushViewController(repoDetailsVC!, animated: true)
    }
    
  

    @IBAction func openRepoSelected(_ sender: UIButton) {

        performSegue(withIdentifier: "openWebKit", sender: self)

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let myDestincationViewController = (segue.destination as? RepositoryWebViewController) {
            myDestincationViewController.repositoryRepoUrl = selectedRepository.html_url
        }
    }
    @IBAction func unwindFromContributorDetails(segue : UIStoryboardSegue){
        
    }
    
}



class ContributorsCell: UICollectionViewCell {
    
    @IBOutlet weak var contibutorsImage: UIImageView!
    
    @IBOutlet weak var contributorName: UILabel!
}
