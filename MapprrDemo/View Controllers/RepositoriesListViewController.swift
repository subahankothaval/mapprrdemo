//
//  RepositoriesListViewController.swift
//  MapprrDemo
//
//  Created by SUBAHAN on 22/11/18.
//  Copyright © 2018 SUBAHAN. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class RepositoriesListViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate {
    @IBOutlet weak var repositoriesTableView: UITableView!
    
    @IBOutlet weak var repositarySearch: UISearchBar!
    
    @IBOutlet weak var filterView: UIView!
    
    @IBOutlet weak var orderSegment: UISegmentedControl!
    @IBOutlet weak var sortSegment: UISegmentedControl!
    
    var orderFilter:String! = "desc"
    var sortFilter:String! = "stars"
    
    var SearchBarValue:String!
    var searchActive : Bool = false

    
    var repositoriesList : [GitHubResponse.Items] = []{
        didSet{
            repositoriesTableView.reloadData()
        }
    }
    var searchResponseModel : GitHubResponse?{
        didSet{
            repositoriesList = searchResponseModel?.items ?? []
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Mapprr"
        self.filterView.isHidden = true
        repositarySearch.showsCancelButton = false
        repositoriesTableView.tableFooterView = UIView.init(frame: .zero)
        repositoriesTableView.estimatedRowHeight = 190
        repositoriesTableView.rowHeight = UITableView.automaticDimension
        
        
        repositoriesList = []
        
    }
    
    @IBAction func orderSegmentIndexChanged(_ sender: UISegmentedControl) {
        switch orderSegment.selectedSegmentIndex
        {
        case 0:
            orderFilter = "desc"
        case 1:
           orderFilter = "asc"
        default:
            break;
        }
        if(SearchBarValue != nil){
             getResultsFor(searchTerm : SearchBarValue,filterOrder : orderFilter,filterSort: sortFilter)
        }
       
    }
    
    @IBAction func sortSegmentIndexChanged(_ sender: UISegmentedControl) {
        switch sortSegment.selectedSegmentIndex
        {
        case 0:
            sortFilter = "stars"
        case 1:
            sortFilter = "forks"
        case 2:
           sortFilter = "updated"
        default:
            break;
        }
        
        if(SearchBarValue != nil){
            getResultsFor(searchTerm : SearchBarValue,filterOrder : orderFilter,filterSort: sortFilter)
        }
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return repositoriesList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let repositoryCell = tableView.dequeueReusableCell(withIdentifier: "RepositoryTableCell", for: indexPath) as? RepositorieListTableViewCell
        let currentRepo = repositoriesList[indexPath.row]
        repositoryCell?.nameLabel.text = "Name: " + (currentRepo.name ?? "")
        repositoryCell?.fullNameLabel.text = "Full Name: " + (currentRepo.full_name ?? "")
        repositoryCell?.watcherCountLabel.text = "Watcher Count: " + "\(currentRepo.watchers_count ?? 0)"
        repositoryCell?.commitCountLabel.text = "Commit Count: " + "\(currentRepo.forks_count ?? 0)"
        repositoryCell?.repoImageView.af_setImage(withURL: URL.init(string: currentRepo.owner?.avatar_url ?? "")!)
        return repositoryCell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let currentRepo = repositoriesList[indexPath.row]
        let repoDetailsVC = UIStoryboard.init(name: "MapprrDemo", bundle: nil).instantiateViewController(withIdentifier: "RepositoryDetailsViewControllerID") as? RepositoryDetailsViewController
        _ = repoDetailsVC?.view
        repoDetailsVC?.selectedRepository = currentRepo
        self.navigationController?.pushViewController(repoDetailsVC!, animated: true)
    }
    

    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true
        self.filterView.isHidden = true

    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        
        searchBar.text = nil
         orderFilter = "desc"
         sortFilter = "stars"
        //
        orderSegment.selectedSegmentIndex = 0
        sortSegment.selectedSegmentIndex = 0
         self.filterView.isHidden = true
        searchBar.resignFirstResponder()
        repositoriesTableView.resignFirstResponder()
        repositarySearch.showsCancelButton = false
        repositoriesTableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
    }
    
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        return true
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        self.searchActive = true;

        SearchBarValue = searchText
        
        getResultsFor(searchTerm : SearchBarValue,filterOrder : orderFilter,filterSort: sortFilter)

        
    }
 
    @IBAction func sortList(_ sender: Any) {

        if(self.filterView.isHidden == true){
            self.filterView.isHidden = false
        }else{
            self.filterView.isHidden = true
        }

    }
    
    func getResultsFor(searchTerm : String,filterOrder : String,filterSort : String){
        let todoEndpoint: String = "https://api.github.com/search/repositories?q=\(searchTerm)&sort=\(filterOrder)&order=\(filterSort)"
        let sv = UIViewController.displaySpinner(onView: self.view)
        Alamofire.request(todoEndpoint)
            .responseJSON {[weak self] response in
                // check for errors
                guard response.result.error == nil else {
                    // got an error in getting the data, need to handle it
                    print("error calling GET on /todos/1")
                    print(response.result.error!)
                    UIViewController.removeSpinner(spinner: sv)
                    return
                }
                
                // make sure we got some JSON since that's what we expect
                if let json = response.result.value as? [String: Any]{
                    if let responseObject = GitHubResponse.init(dictionary: json as NSDictionary){
                        self?.searchResponseModel = responseObject
                    }
                    UIViewController.removeSpinner(spinner: sv)
                } else {
                    print("didn't get  object as JSON from API")
                    if let error = response.result.error {
                        print("Error: \(error)")
                        UIViewController.removeSpinner(spinner: sv)
                    }
                    return
                }
                
        }
    }
}

class RepositorieListTableViewCell : UITableViewCell{
    @IBOutlet weak var contentContainer: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        contentContainer.layer.borderColor = UIColor.black.cgColor
        contentContainer.layer.borderWidth = 1
    }
    
    @IBOutlet weak var repoImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var fullNameLabel: UILabel!
    @IBOutlet weak var watcherCountLabel: UILabel!
    @IBOutlet weak var commitCountLabel: UILabel!
    
}
